# Technologies

## Distributed File Systems

- HDFS (Hadoop Distributed File System)
- S3 (Amazon)
- Azure Block Storage

| Name       | Function                                                |
| ---------- | ------------------------------------------------------- |
| HDFS       | Distributed File System                                 |
| S3         | Distributed File System                                 |
| Azure      | Distributed File System                                 |
| HBase      | Wide Column Storage                                     |
| Bigtable   | Wide Column Storage                                     |
| Yarn       | Resource Negotiator                                     |
| Map Reduce | System to process huge amounts of key value pairs       |
| Spark      | System to process huge amounts of generic data          |
| Hadoop     | Data System, using HDFS, HBase, Yarn, Map Reduce, Spark |

# Key Words

## General

- Data Independence -> Data Layers are independent from changes in the lower level

## Integrity

- Tabular Integrity -> All rows have values for all columns
- Domain Integrity -> All cells in a column have the same type
- First Normal Form - Atomic Integrity -> No nestedness

## Relational Algebra

- Selection -> Select Rows
- Projection -> Select Columns
- Grouping -> Group by one column and aggregate (transform to single value) other columns
- Sorting -> Sort by one column
- Cartesian Product -> Take two tables, and create all possible concatenations of all rows
- Join -> Join

## Transactions (ACID)

- Atomicity: A change is either done completely or not at all
- Consistency: Certain constraints on the data are fulfilled
- Insulation: Data looks the same to everybody, it seams to them that they are the only ones
- Durability: No data loss

# Distributed File System

- Used for really large files
- allows concurrent access

## Storage

- uses file system
- files are split into blocks, which are distributed and replicated over data nodes
- to query a file, the name node is queried, which presents a list of the block locations

## Read/Write

- reading is possible concurrently
- writing is only possible in append mode

# HDFS

## Structure

### Namenode

Stores:

- Mapping from file to blocks
- Mapping from blocks to datanodes
- File paths

### Datanode

- Stores Blocks
- Reports heartbeat every few seconds
- Reports blocks every few hours
- Reports newly added blocks

## Read

- Request to namenode, it sends block numbers and locations
- Request to datanodes, it sends data

## Write

- Request to namenode, it sends future block numbers and locations
- Request to first datanode, it contacts replica nodes
- Send block to first datanode, it replicates it
- Datanodes report to namenode
- Repeat
- If namenode sees that everything is replicated with the minimum required amount, ACK to client
- Further asynchronous replication

# Syntax

- JSON and XML used
- XML supports namespaces to distinguish between different tags with the same name

## XML

Default are assigned as follows:

```xml
<elem xmlns="www.eth-lerngruppe.ch">
    <a/>
</elem>
```

Multiple namespaces are assigned as follows:

```xml
<elem
    xmlns:ns1="www.eth-lerngruppe.ch/1"
    xmlns:ns2="www.eth-lerngruppe.ch/2"
>
    <a:ns1 calvin:ns1="agi"/>
    <a:ns2 calvin:ns2="iia"/>
</elem>
```

# Wide Column Storage

Data is no longer split into multiple tables. It is saved in a single, already 'joint' table. This leads to duplicates, but eases access.

Columns are grouped into column families.

Multiple rows are called a region

A column family of a region is called a store. A represents is the file that is saved on HDFS, as an HFile of key value pairs of single cells.

An HFile is further split into HBlocks, which are indexed inside an HFile.

Cells are saved as key value pairs, where the key consists of the concatenation of

- row length
- row
- column family length
- column family
- column quantifier
- timestamp for versioning
- key type, flag for deletion etc.

## Memory

Everything is saved on HDFS, however, new entries are cached in a separate HDFS block. They are held in a sorted log tree and wrote back to the main system from time to time.

# Validation

## XML Schemas

Are defined with

```xml
<a xsi:schemaLocation="https://www.w3schools.com/xml_note.xsd"> agi </a>
```

in the top level tag

They contain either simple elements that contain only text, however, this text can also be typed (i.e. xs:number, xs:data, etc.)

There are also complex Types, they contain either a `<sequence></sequence>` tag, containing other elements, or just several `<xs:attribute name="OrderID" type="xs:int"/>` tags with name and type.

## JSON Schema

It can be defined with

```json
{
  "$schema": "https://www.w3schools.com/json_note.json",
  "name": "noah",
  "iq": "inf"
}
```

# Map Reduce

System to process large amounts of data

## Job Tracker

A Job Tracker organizes the whole thing. It assigns TaskTrackers to map or reduce tasks. It also provides fault tolerance, scheduling, monitoring, etc.

## Map Job

a single (key1, value) pair is mapped to another (keyA, value) pair.

## Sorting

All pairs with the same key are sent to the same reduce slot.

## Combine

It is sometimes also possible to do a reduce step already at the map slot, this is called combine

## Reduce Job

All pairs with the same key are now reduced to a single pair.

# YARN

It consists of a Resource Manager doing the high level work, and Node Managers which offer different containers to run stuff.

## Resource Manager

It is responsible for contact with the client. It takes requests from the client

## Node Manager
It provides containers. Sends node status periodically.

## Application Master
A container assigned by the resource manager. It requests additional containers from the resource master. It then takes these from the corresponding node masters. These are then monitored directly by the application master. After termination it is freed to the resource manager again.

# Spark

A more general system than map reduce.

## RDD
Resilient Distributed Dataset
A list of data units. There is no constraint on what types of units are used.

## Transformations
Tasks that perform intermediate steps.



## Actions
Tasks that preform final steps.

## Dataframes

# Optimizations
- let two CPU's do the same job at the same time, to avoid one CPU taking for ages